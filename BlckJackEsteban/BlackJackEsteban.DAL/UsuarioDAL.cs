﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlackJackEsteban.ENTITIES;
using Npgsql;

namespace BlackJackEsteban.DAL
{
    public class UsuarioDAL
    {
        /// <summary>
        /// trae de la base de datos el usuario completo
        /// </summary>
        /// <param name="us">ususario de la plataforma</param>
        /// <returns>usuario completo</returns>
        public EUsuario Login(EUsuario us)
        {
            try
            {
                EUsuario nuevo = new EUsuario();
                string sql = "SELECT id, id_plataforma, nombre, url_foto, email, plataforma, saldo, partidas " +
                    " FROM app.usuarios WHERE id_plataforma LIKE '" + us.ID_Cuenta + "'";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    try
                    {
                        Console.WriteLine(cmd.CommandText);
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader.GetInt32(0) != 0)
                            {
                                nuevo = ArmarUsuario(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                    }
                }
                if (nuevo.ID != 0)
                {
                    return nuevo;
                }
                else
                {
                    Insertar(us);
                }
                return us;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "-en login de dal");
                return null;
            }

        }

        /// <summary>
        /// actualiza el saldo
        /// </summary>
        /// <param name="res">saldo</param>
        /// <param name="user">usuario</param>
        public void QuitarSaldo(int res, EUsuario user)
        {
            string sql = "UPDATE app.usuarios SET saldo = " + res + " WHERE id = '" + user.ID + "'";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// actualiza el saldo
        /// </summary>
        /// <param name="user">usuario</param>
        public void AumentarSaldo(EUsuario user)
        {
            string sql = "UPDATE app.usuarios SET saldo = " + user.Saldo + " WHERE id = '" + user.ID + "'";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ingresa apuestas
        /// </summary>
        /// <param name="value">apuestas</param>
        /// <param name="us">usuarios</param>
        public void IngreApuesta(decimal value, EUsuario us)
        {
            string sql = "UPDATE app.usuarios SET apuesta = "+value+" WHERE id = '" + us.ID + "'";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// inserta un usuario
        /// </summary>
        /// <param name="us">usuario</param>
        /// <returns></returns>
        private EUsuario Insertar(EUsuario us)
        {
            string sql = "INSERT INTO app.usuarios( id_plataforma, nombre, url_foto, email, plataforma) VALUES(@idp, @nom, @url, @ema, @pla)";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@idp", us.ID_Cuenta);
                cmd.Parameters.AddWithValue("@nom", us.Nombre);
                cmd.Parameters.AddWithValue("@url", us.UrlFoto);
                cmd.Parameters.AddWithValue("@ema", us.Email);
                cmd.Parameters.AddWithValue("@pla", us.Plataforma);
                us.ID = (int)cmd.ExecuteScalar();

            }
            return us;
        }

        /// <summary>
        /// arma un usuario
        /// </summary>
        /// <param name="reader">lector de base de datos</param>
        /// <returns>un usuario</returns>
        private EUsuario ArmarUsuario(NpgsqlDataReader reader)
        {
            EUsuario temp = new EUsuario();
            temp.ID = reader.GetInt32(0);
            temp.ID_Cuenta = reader.GetString(1);
            temp.Nombre = reader.GetString(2);
            temp.UrlFoto = reader.GetString(3);
            temp.Email = reader.GetString(4);
            temp.Plataforma = reader.GetString(5);
            temp.Saldo = reader.GetInt32(6);
            temp.Partidas = reader.GetInt32(7);
            return temp;
        }
    }
}
