﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using BlackJackEsteban.ENTITIES;
using Npgsql;

namespace BlackJackEsteban.DAL
{
    public class AppDAL
    {
        /// <summary>
        /// envia el correo con los datos del usuario 
        /// </summary>
        /// <param name="salap">sala para el usuario</param>
        /// <param name="u">el usuario al cual se le enviara el mensaje</param>
        public void EnviarCorreo(ESala salap, EUsuario u)
        {
            try
            {
                string email = "emurillor@est.utn.ac.cr";
                string password = "Kraken2109";
                string asunto = "Información de su sala privada de BlackJack";
                string mensaje = "Su sala tiene las siguientes medidas de seguridad: id: " + salap.ID + ", contraseña: " + salap.Password;
                string destinatario = u.Email;

                MailMessage mail = new MailMessage(email, destinatario, asunto, mensaje);
                SmtpClient server = new SmtpClient("smtp.live.com");
                NetworkCredential credenciales = new NetworkCredential(email, password);
                server.Credentials = credenciales;
                server.EnableSsl = true;

                server.Send(mail);
                mail.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// trae los datos de la partida constantemente
        /// </summary>
        /// <param name="par">sala</param>
        /// <returns>sala actualizada</returns>
        public ESala ActualizarPartida(ESala par)
        {
            try
            {
                ESala nuevo = new ESala();
                string sql = "SELECT id, contra, j1, j2, j3, j4, j5, j6, j7, turno, baraja, cupos" +
                    " FROM app.partida WHERE id = " + par.ID + " AND contra = '" + par.Password + "'";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    try
                    {
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            Console.WriteLine(reader.GetInt32(0));
                            if (reader.GetInt32(0) != 0)
                            {

                                nuevo = ArmarSala(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                if (nuevo.ID != 0)
                {

                    return nuevo;
                }
                else
                {
                    //nuevo = CrearSala();
                    //return nuevo;
                }
                return nuevo;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "-en login de dal");
                return new ESala();
            }
        }

        /// <summary>
        /// trae el id de la baraja y actualiza la baraja o sube el id de la actual
        /// </summary>
        /// <param name="us">usuario</param>
        /// <param name="deck">id del deck</param>
        /// <returns></returns>
        public string PedirBaraja(EUsuario us, DeckOfCardsEntities.RootobjectB deck)
        {
            string temp = String.Empty;
            try
            {
                string sql = "SELECT baraja FROM app.partida where id =" + us.part_act.ID;
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        temp = reader.GetString(0);
                    }
                }

                return temp;
            }
            catch (Exception ex)
            {
                if (temp.Equals(""))
                {
                    Console.WriteLine(ex.Message);
                    try
                    {
                        string sql = "UPDATE app.partida SET baraja= @bar WHERE id= @id";
                        using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                        {
                            conn.Open();
                            NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                            cmd.Parameters.AddWithValue("@bar", deck.deck_id);
                            cmd.Parameters.AddWithValue("@ID", us.part_act.ID);
                            cmd.ExecuteNonQuery();
                        }
                    }

                    catch (Exception ex1)
                    {
                        Console.WriteLine(ex1.Message);
                    }
                }
            }
            return deck.deck_id;
        }

        /// <summary>
        /// pide las cartas del dealer
        /// </summary>
        /// <param name="part_act">partida</param>
        /// <returns>lista con cartas</returns>
        public List<string> PedirCartasDealer(ESala part_act)
        {
            List<string> cartas = new List<string>();
            try
            {
                string sql = "SELECT carta1, carta2, carta3, carta4, carta5  FROM app.partida WHERE id = " + part_act.ID
                    + " AND contra LIKE '" + part_act.Password + "' ";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    try
                    {
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            if (reader.GetInt32(0) != 0)
                            {

                                cartas = ArmarDealer(reader);
                            }
                        }
                        return cartas;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return cartas;
        }

        public void DejarPartida(EUsuario user, string pos)
        {
            try
            {
                Console.WriteLine(pos);
                string sql = "UPDATE app.partida SET "+pos+ "= @pos, cupos=@cup WHERE id= @id";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@pos", 0);
                    cmd.Parameters.AddWithValue("@cup", (user.part_act.cupos-1));
                    cmd.Parameters.AddWithValue("@ID", user.part_act.ID);
                    cmd.ExecuteNonQuery();
                }
            }

            catch (Exception ex1)
            {
                Console.WriteLine(ex1.Message);
            }
        }

        /// <summary>
        /// arma el dealer
        /// </summary>
        /// <param name="reader">respuesta de la base</param>
        /// <returns>lista de dealer</returns>
        private List<string> ArmarDealer(NpgsqlDataReader reader)
        {
            List<string> temp = new List<string>();
            try
            {
                for (int i = 0; i < 5; i++)
                {
                    if (String.IsNullOrWhiteSpace(reader.GetString(i)) || String.IsNullOrEmpty(reader.GetString(i)))
                    {
                        temp.Add(reader.GetString(i));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }

        /// <summary>
        /// actualiza el turno
        /// </summary>
        /// <param name="sal">sala</param>
        public void TerminarTurno(ESala sal)
        {
            string sql = "UPDATE app.partida SET turno = @bar WHERE id= @id";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@bar", sal.Turno++);
                cmd.Parameters.AddWithValue("@ID", sal.ID);
                cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// ingresa el usuario a la mesa de juego
        /// </summary>
        /// <param name="idp">identificador de la partida</param>
        /// <param name="idu">identificador del usuario</param>
        /// <param name="cupo">cantidad de cupos restantes</param>
        public void InsertarUsuario(int idp, int idu, int cupo)
        {
            string sql = "UPDATE app.partida SET ";
            switch (cupo)
            {
                case 0:
                    sql += "j1=@idu, cupos=1";
                    break;
                case 1:
                    sql += "j2=@idu, cupos=2";
                    break;
                case 2:
                    sql += "j3=@idu, cupos=3";
                    break;
                case 3:
                    sql += "j4=@idu, cupos=4";
                    break;
                case 4:
                    sql += "j5=@idu, cupos=5";
                    break;
                case 5:
                    sql += "j6=@idu, cupos=6";
                    break;
                case 6:
                    sql += "j7=@idu, cupos=7";
                    break;
                default:
                    break;
            }
            sql += " WHERE id = @idp";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@idu", idu);
                cmd.Parameters.AddWithValue("@idp", idp);
                cmd.ExecuteNonQuery();

            }
        }

        /// <summary>
        /// busca una sala privada según si id y su contraseña 
        /// </summary>
        /// <param name="id">identificador de la sala</param>
        /// <param name="con">contraseña de la sala</param>
        /// <returns></returns>
        public ESala BuscarSala(int id, string con)
        {
            ESala temp = new ESala();
            string sql = "SELECT id, contra, j1, j2, j3, j4, j5, j6, j7, turno, baraja, cupos FROM app.partida WHERE id = " + id + " AND contra LIKE '" + con + "' and cupos <7";
            using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
            {
                conn.Open();
                NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                //cmd.Parameters.AddWithValue("@id", id);
                //cmd.Parameters.AddWithValue("@con", con);
                try
                {
                    //Console.WriteLine(cmd.CommandText);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        Console.WriteLine(reader.GetInt32(0));
                        if (reader.GetInt32(0) != 0)
                        {

                            temp = ArmarSala(reader);
                        }
                    }
                    return temp;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// se encarga de solicitar o mandar a crear una sala cualquiera para un usuario casual
        /// </summary>
        /// <returns>la sala encontrada</returns>
        public ESala PedirSalaUsuario()
        {
            try
            {
                ESala nuevo = new ESala();
                string sql = "SELECT id, contra, j1, j2, j3, j4, j5, j6, j7, turno, baraja, cupos" +
                    " FROM app.partida WHERE cupos<7";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    try
                    {
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            Console.WriteLine(reader.GetInt32(0));
                            if (reader.GetInt32(0) != 0)
                            {

                                nuevo = ArmarSala(reader);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                if (nuevo.ID != 0)
                {

                    return nuevo;
                }
                else
                {
                    nuevo = CrearSala();
                    return nuevo;
                }
                return nuevo;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "-en login de dal");
                return new ESala();
            }
        }

        /// <summary>
        /// busca una sala privada o mandar a crear una nueva sala
        /// </summary>
        /// <param name="u">el usuario que solicita la sala</param>
        /// <returns>la sala encontrada</returns>
        public ESala PedirSalaPrivada(EUsuario u)
        {
            try
            {
                ESala nuevo = new ESala();
                string sql = "SELECT id, contra, j1, j2, j3, j4, j5, j6, j7, turno, baraja, cupos" +
                    " FROM app.partida WHERE cupos=0";
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    try
                    {
                        Console.WriteLine(cmd.CommandText);
                        NpgsqlDataReader reader = cmd.ExecuteReader();
                        //Console.WriteLine(reader.GetInt32(0));

                        if (reader.Read())
                        {
                            Console.WriteLine(reader.GetInt32(0));
                            if (reader.GetInt32(0) != 0)
                            {

                                nuevo = ArmarSala(reader);
                            }
                        }
                        return nuevo;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                }
                if (nuevo.ID != 0)
                {

                    return nuevo;
                }
                else
                {
                    nuevo = CrearSala();
                    return nuevo;
                }
                return new ESala();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " -en login de dal");
                return new ESala();
            }
        }

        /// <summary>
        /// crea una sala según la demanda y la cantidad de salas disponibles
        /// </summary>
        /// <returns>los datos de la nueva sala</returns>
        private ESala CrearSala()
        {
            try
            {
                ESala temp = new ESala();
                string sql = "INSERT INTO app.partida(contra, j1, j2, j3, j4, j5, j6, j7, cupos ) VALUES( @Con, @j, @j, @j, @j, @j, @j, @j, @j)";
                string con = CrearContra();
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {
                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@con", con);
                    cmd.Parameters.AddWithValue("@j", 0);
                    temp.ID = (int)cmd.ExecuteScalar();
                    Console.WriteLine(temp.ID);
                }
                return temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new ESala();
        }

        /// <summary>
        /// se encarga de crear una contraseña aleatoria para la sala, se me complicó hacerla en el bol
        /// debido a que solo es llamado desde el método de CrearSala() y no se permite una referencia del DAL al BOL
        /// </summary>
        /// <returns>la nueva contraseña aleatoria</returns>
        private string CrearContra()
        {
            Random rdn = new Random();
            string caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890%$#@";
            int longitud = caracteres.Length;
            char letra;
            int longitudContrasenia = 10;
            string contraseniaAleatoria = string.Empty;
            for (int i = 0; i < longitudContrasenia; i++)
            {
                letra = caracteres[rdn.Next(longitud)];
                contraseniaAleatoria += letra.ToString();
            }
            return contraseniaAleatoria;
        }

        /// <summary>
        /// se encarga de agregar la información traida de la base de datos a la entidad de ESala y retornarla
        /// </summary>
        /// <param name="reader">es el lector de datos de la base de datos</param>
        /// <returns>la nueva sala</returns>
        private ESala ArmarSala(NpgsqlDataReader reader)
        {
            List<EUsuario> t = new List<EUsuario>();
            t.Add(TraerUsario(reader.GetInt32(2)));
            t.Add(TraerUsario(reader.GetInt32(3)));
            t.Add(TraerUsario(reader.GetInt32(4)));
            t.Add(TraerUsario(reader.GetInt32(5)));
            t.Add(TraerUsario(reader.GetInt32(6)));
            t.Add(TraerUsario(reader.GetInt32(7)));
            t.Add(TraerUsario(reader.GetInt32(8)));

            ESala temp = new ESala();

            try
            {
                temp.ID = reader.GetInt32(0);
                temp.Password = reader.GetString(1);
                temp.Jugadores = t;
                temp.Turno = reader.GetInt32(9);
                try
                {

                    temp.Baraja = reader.GetString(10);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                temp.cupos = reader.GetInt32(11);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return temp;
        }

        /// <summary>
        /// se encarga de traer los usuarios de la base de datos que estan en la sala del jugador local y 
        /// agregarlos a la sala
        /// </summary>
        /// <param name="v">es el id de usuario de la tabla de la sala</param>
        /// <returns>el nuevo usuario</returns>
        private EUsuario TraerUsario(int v)
        {
            try
            {
                EUsuario nuevo = new EUsuario();
                string sql = "SELECT id, id_plataforma, nombre, url_foto, email, plataforma, saldo, partidas " +
                    " FROM app.usuarios WHERE id = " + v;
                using (NpgsqlConnection conn = new NpgsqlConnection(Conexion.conStr))
                {

                    conn.Open();
                    NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
                    NpgsqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        if (reader.GetInt32(0) != 0)
                        {
                            nuevo = ArmarUsuario(reader);
                        }
                    }

                }
                return nuevo;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return new EUsuario();
        }

        /// <summary>
        /// según el lector de base de datos se encarga de crear un nuevo EUsuario según sus datos
        /// </summary>
        /// <param name="reader">lector de la base de datos</param>
        /// <returns>el nuevo usuario</returns>
        public EUsuario ArmarUsuario(NpgsqlDataReader reader)
        {
            try
            {
                EUsuario temp = new EUsuario
                {
                    ID = reader.GetInt32(0),
                    ID_Cuenta = reader.GetString(1),
                    Nombre = reader.GetString(2),
                    UrlFoto = reader.GetString(3),
                    Email = reader.GetString(4),
                    Plataforma = reader.GetString(5),
                    Saldo = reader.GetInt32(6),
                    Partidas = reader.GetInt32(7)
                };
                return temp;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return new EUsuario();
            }
        }
    }
}