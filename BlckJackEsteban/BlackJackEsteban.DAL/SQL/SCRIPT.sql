--CREATE DATABASE black_jack_esteban;

--CREATE SCHEMA app;

CREATE TABLE app.usuarios
(
	id SERIAL primary KEY,
	id_plataforma TEXT NOT NULL UNIQUE,
	nombre TEXT,
	url_foto TEXT,
	email TEXT NOT NULL,
	plataforma TEXT NOT NULL,
	saldo NUMERIC DEFAULT 0,
	partidas NUMERIC DEFAULT 0,
	apuesta bigint,
    carta text,
    gana boolean,
	partida_act numeric
);

CREATE TABLE app.partida
( 
	id SERIAL PRIMARY KEY,
	contra TEXT NOT NULL,
	j1 NUMERIC default 0,
	j2 NUMERIC default 0, 
	j3 NUMERIC default 0, 
	j4 NUMERIC default 0,
	j5 NUMERIC default 0,
	j6 NUMERIC default 0, 
	j7 NUMERIC default 0,
	turno NUMERIC default 1,
	cupos NUMERIC DEFAULT 0,
	baraja TEXT,
	carta1 TEXT,
	carta2 TEXT,
	carta3 TEXT,
	carta4 TEXT,
	carta5 TEXT
);