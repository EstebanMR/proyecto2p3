﻿using BlackJackEsteban.BOL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlckJackEsteban
{
    public partial class frmApuesta : Form
    {
        UsuarioBOL usBol;
        int restante;
        public frmApuesta(UsuarioBOL usBol)
        {
            InitializeComponent();
            this.usBol = usBol;
        }

        private void frmApuesta_Load(object sender, EventArgs e)
        {
            label1.Text = "Apuesta: "+usBol.user.Apuesta;
            restante = usBol.user.Saldo;
        }

        /// <summary>
        /// aumenta en 5 la apuesta y resta 5 colones a el saldo del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (usBol.user.Apuesta + 5 <= usBol.user.Saldo)
            {
                usBol.user.Apuesta += 5;
                restante -= 5;
                label1.Text = "Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, no cuenta con el saldo suficiente para realizar la apuesta", "Apuesta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// aumenta en 25 la apuesta y resta 25 colones a el saldo del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            if (usBol.user.Apuesta + 25 <= usBol.user.Saldo)
            {
                restante -= 25;
                usBol.user.Apuesta += 25;
                label1.Text = "Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, no cuenta con el saldo suficiente para realizar la apuesta", "Apuesta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// aumenta en 100 la apuesta y resta 100 colones a el saldo del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            if (usBol.user.Apuesta + 100 <= usBol.user.Saldo)
            {
                restante -= 100;
                usBol.user.Apuesta += 100;
                label1.Text = "Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, no cuenta con el saldo suficiente para realizar la apuesta", "Apuesta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// aumenta en 500 la apuesta y resta 500 colones a el saldo del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox5_Click(object sender, EventArgs e)
        {
            if (usBol.user.Apuesta + 500 <= usBol.user.Saldo)
            {
                restante -= 500;
                usBol.user.Apuesta += 500;
                label1.Text = "Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, no cuenta con el saldo suficiente para realizar la apuesta", "Apuesta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// aumenta en 1000 la apuesta y resta 1000 colones a el saldo del jugador
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            if (usBol.user.Apuesta + 1000<=usBol.user.Saldo)
            {
                restante -= 1000;
                usBol.user.Apuesta += 1000;
                label1.Text = "Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, no cuenta con el saldo suficiente para realizar la apuesta", "Apuesta", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        /// <summary>
        /// boton de all in
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            usBol.user.Apuesta = usBol.user.Saldo;
            usBol.user.Saldo = 0;
            restante = 0;
            label1.Text = "Apuesta: " + usBol.user.Apuesta;
        }

        /// <summary>
        /// ingresa la apuest, quita el saldo y cierra del frame
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            usBol.IngreApuesta(usBol.user.Apuesta, usBol.user);
            usBol.QuitarSaldo(restante);
            Close();
        }
    }
}
