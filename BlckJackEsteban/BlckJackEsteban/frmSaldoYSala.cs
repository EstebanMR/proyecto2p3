﻿using BlackJackEsteban.BOL;
using System;
using System.Windows.Forms;

namespace BlckJackEsteban
{
    public partial class frmSaldoYSala : Form
    {
        AppBOL log;
        UsuarioBOL UsBOL;

        public frmSaldoYSala(UsuarioBOL UsBOL)
        {
            InitializeComponent();
            this.UsBOL = UsBOL;
            log = new AppBOL();
            nupApuesta.Value = UsBOL.user.Saldo;
        }

        /// <summary>
        /// busca sala normal 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                UsBOL.user.Saldo = (int)nupApuesta.Value;
                UsBOL.AumentarSaldo();
                UsBOL.user.part_act = log.BuscarPartida();
                if (UsBOL.user.part_act != null)
                {
                    log.InsertarEnPartida(UsBOL.user.part_act.ID, UsBOL.user.ID, UsBOL.user.part_act.cupos);
                    frmMesa frm = new frmMesa(UsBOL);
                    Close();
                    frm.ShowDialog();
                }
                else
                {

                }

            }
            catch (Exception ex)
            {

                throw;
            }
        }

        /// <summary>
        /// muestra el frame para que el usuario que busca una sala privada de creada por otra persona
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                UsBOL.user.Saldo = (int)nupApuesta.Value;
                UsBOL.user.part_act = log.CrearPrivada(UsBOL.user);
                UsBOL.AumentarSaldo();
                MessageBox.Show("Los datos de la sala de juego se han enviado a su correo, por favor reviselo", "Crear sala", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                log.InsertarEnPartida(UsBOL.user.part_act.ID, UsBOL.user.ID, UsBOL.user.part_act.cupos);
                frmMesa frm = new frmMesa(UsBOL);
                Close();
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Ha ocurrido un error al enviar el correo con los datos, por favor intentelo más tarde", "Crear sala", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// se encarga de buscar o crear una sala privada
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                UsBOL.user.Saldo = (int)nupApuesta.Value;
                UsBOL.AumentarSaldo();
                frmIngresoDatosSalaPrivada frm = new frmIngresoDatosSalaPrivada();
                int id = 0;
                string con = String.Empty;
                try
                {
                    frm.ShowDialog();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Ha ocurrido un error, por favor intente de nuevo");
                }
                try
                {
                    id = frm.GetId();
                    con = frm.GetContra();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Ha ocurrido un error, por favor intente de nuevo");
                }
                try
                {
                    UsBOL.user.part_act = log.BuscarPrivada(id, con);
                    if (UsBOL.user.part_act.ID != 0)
                    {
                        log.InsertarEnPartida(UsBOL.user.part_act.ID, UsBOL.user.ID, UsBOL.user.part_act.cupos);
                        frmMesa frm1 = new frmMesa(UsBOL);
                        Close();
                        frm1.ShowDialog();

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show("Lo sentimos esta sala esta llena");
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                MessageBox.Show("Ha ocurrido un error, por favor intente de nuevo");
            }
        }

        /// <summary>
        /// aumenta el saldo del usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            if ((int)nupApuesta.Value>UsBOL.user.Saldo)
            {
                UsBOL.user.Saldo = (int)nupApuesta.Value;
                UsBOL.AumentarSaldo();
            }
        }

        private void frmSaldoYSala_Load(object sender, EventArgs e)
        {

        }
    }
}
