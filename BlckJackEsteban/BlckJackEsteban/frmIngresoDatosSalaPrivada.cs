﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlckJackEsteban
{
    public partial class frmIngresoDatosSalaPrivada : Form
    {
        private int id;
        private string contra;
        public frmIngresoDatosSalaPrivada()
        {
            InitializeComponent();
            id = 0;
            contra = String.Empty;
        }

        /// <summary>
        /// lee los datos ingresados en la interfaz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            id = (int)numericUpDown1.Value;
            contra = textBox2.Text;
            Close();
        }

        /// <summary>
        /// devuelve el id de la sala
        /// </summary>
        /// <returns></returns>
        public int GetId()
        {
            return id;
        }

        /// <summary>
        /// devuelve la contraseña de la sala
        /// </summary>
        /// <returns></returns>
        public string GetContra()
        {
            return contra;
        }
    }

}
