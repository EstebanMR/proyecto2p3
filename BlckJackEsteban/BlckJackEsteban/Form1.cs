﻿using BlackJackEsteban.BOL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlckJackEsteban
{
    public partial class Form1 : Form
    {
        public UsuarioBOL UsBol;
        List<String> InfoGoogle;
        AppBOL log = new AppBOL();
        const string clientID = "415056751552-hmqhrfh66639kbfdn1kctfaqhnsb0k03.apps.googleusercontent.com";
        const string clientSecret = "l3HBNBASc-Sy6B6QLcHNIqxV";
        const string scope = "openid email profile";
        const string authorizationEndpoint = "https://accounts.google.com/o/oauth2/v2/auth";
        const string tokenEndpoint = "https://www.googleapis.com/oauth2/v4/token";
        const string userInfoEndpoint = "https://www.googleapis.com/oauth2/v3/userinfo";

        public Form1()
        {
            InitializeComponent();
            InfoGoogle = new List<string>();
            UsBol = new UsuarioBOL();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Inicio de sesion con google
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        async private void pictureBox1_ClickAsync(object sender, EventArgs e)
        {
            // Generates state and PKCE values.
            string state = log.randomDataBase64url(32);
            string code_verifier = log.randomDataBase64url(32);
            string code_challenge = log.base64urlencodeNoPadding(AppBOL.sha256(code_verifier));
            const string code_challenge_method = "S256";

            // Creates a redirect URI using an available port on the loopback address.
            string redirectURI = string.Format("http://{0}:{1}/", IPAddress.Loopback, AppBOL.GetRandomUnusedPort());

            // Creates an HttpListener to listen for requests on that redirect URI.
            var http = new HttpListener();
            http.Prefixes.Add(redirectURI);
            http.Start();

            // Creates the OAuth 2.0 authorization request.
            string authorizationRequest = string.Format("{0}?response_type=code&redirect_uri={1}&client_id={2}&state={3}&code_challenge={4}&code_challenge_method={5}&scope={6}",
                authorizationEndpoint,
                System.Uri.EscapeDataString(redirectURI),
                clientID,
                state,
                code_challenge,
                code_challenge_method,
                scope);

            // Opens request in the browser.
            System.Diagnostics.Process.Start(authorizationRequest);

            // Waits for the OAuth authorization response.
            var context = await http.GetContextAsync();

            // Brings this app back to the foreground.
            this.Activate();

            // Sends an HTTP response to the browser.
            var response = context.Response;
            string responseString = string.Format("<html><head><meta http-equiv='refresh' content='10;url=https://google.com'></head><body>Please return to the app.</body></html>");
            var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            response.ContentLength64 = buffer.Length;
            var responseOutput = response.OutputStream;
            Task responseTask = responseOutput.WriteAsync(buffer, 0, buffer.Length).ContinueWith((task) =>
            {
                responseOutput.Close();
                http.Stop();
                Console.WriteLine("HTTP server stopped.");
            });

            // Checks for errors.
            if (context.Request.QueryString.Get("error") != null)
            {
                return;
            }
            if (context.Request.QueryString.Get("code") == null
                || context.Request.QueryString.Get("state") == null)
            {
                return;
            }

            // extracts the code
            var code = context.Request.QueryString.Get("code");
            var incoming_state = context.Request.QueryString.Get("state");

            // Compares the receieved state to the expected value, to ensure that
            // this app made the request which resulted in authorization.
            if (incoming_state != state)
            {
                return;
            }
            // Starts the code exchange at the Token Endpoint.
            performCodeExchange(code, code_verifier, redirectURI);
        }

        /// <summary>
        /// método para traer el json de google con info
        /// </summary>
        /// <param name="code"></param>
        /// <param name="code_verifier"></param>
        /// <param name="redirectURI"></param>
        async private void performCodeExchange(string code, string code_verifier, string redirectURI)
        {
            // builds the  request
            string tokenRequestURI = "https://www.googleapis.com/oauth2/v4/token";
            string tokenRequestBody = string.Format("code={0}&redirect_uri={1}&client_id={2}&code_verifier={3}&client_secret={4}&scope=&grant_type=authorization_code",
                code,
                Uri.EscapeDataString(redirectURI),
                clientID,
                code_verifier,
                clientSecret
                );

            // sends the request
            HttpWebRequest tokenRequest = (HttpWebRequest)WebRequest.Create(tokenRequestURI);
            tokenRequest.Method = "POST";
            tokenRequest.ContentType = "application/x-www-form-urlencoded";
            tokenRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            byte[] _byteVersion = Encoding.ASCII.GetBytes(tokenRequestBody);
            tokenRequest.ContentLength = _byteVersion.Length;
            Stream stream = tokenRequest.GetRequestStream();
            await stream.WriteAsync(_byteVersion, 0, _byteVersion.Length);
            stream.Close();

            try
            {
                // gets the response
                WebResponse tokenResponse = await tokenRequest.GetResponseAsync();
                using (StreamReader reader = new StreamReader(tokenResponse.GetResponseStream()))
                {
                    // reads response body
                    string responseText = await reader.ReadToEndAsync();

                    // converts to dictionary
                    Dictionary<string, string> tokenEndpointDecoded = JsonConvert.DeserializeObject<Dictionary<string, string>>(responseText);

                    string access_token = tokenEndpointDecoded["access_token"];
                    userinfoCall(access_token);
                }
            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = ex.Response as HttpWebResponse;
                    if (response != null)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            // reads response body
                            string responseText = await reader.ReadToEndAsync();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// toma el json de información del usuario
        /// </summary>
        /// <param name="access_token">clave de acceso para pedir información</param>
        async private void userinfoCall(string access_token)
        {
            // builds the  request
            string userinfoRequestURI = "https://www.googleapis.com/oauth2/v3/userinfo";

            // sends the request
            HttpWebRequest userinfoRequest = (HttpWebRequest)WebRequest.Create(userinfoRequestURI);
            userinfoRequest.Method = "GET";
            userinfoRequest.Headers.Add(string.Format("Authorization: Bearer {0}", access_token));
            userinfoRequest.ContentType = "application/x-www-form-urlencoded";
            userinfoRequest.Accept = "Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            // gets the response
            WebResponse userinfoResponse = await userinfoRequest.GetResponseAsync();
            using (StreamReader userinfoResponseReader = new StreamReader(userinfoResponse.GetResponseStream()))
            {
                // reads response body
                string userinfoResponseText = await userinfoResponseReader.ReadToEndAsync();
                llamar(userinfoResponseText);
            }

        }

        /// <summary>
        /// toma el json con la información del json y la agrega a un user
        /// </summary>
        /// <param name="json">documento json a decodificar</param>
        private void llamar(string json)
        {

            JsonTextReader reader = new JsonTextReader(new StringReader(json));
            while (reader.Read())
            {
                if (reader.Value != null)
                {
                    InfoGoogle.Add(reader.Value.ToString());
                }
            }
            for (int i = 0; i < InfoGoogle.Count; i++)
            {
                int num = i;
                num++;
                if (InfoGoogle[i].Equals("sub"))
                {
                    UsBol.user.ID_Cuenta = InfoGoogle[num++].ToString();
                    i++;
                }
                else if (InfoGoogle[i].Equals("name"))
                {
                    UsBol.user.Nombre = InfoGoogle[num++].ToString();
                }
                else if (InfoGoogle[i].Equals("picture"))
                {
                    UsBol.user.UrlFoto = InfoGoogle[num++].ToString();
                }
                else if (InfoGoogle[i].Equals("email"))
                {
                    UsBol.user.Email = InfoGoogle[num++].ToString();
                }
            }
            UsBol.user.Plataforma = "Google";
            pictureBox4.ImageLocation = UsBol.user.UrlFoto.ToString();
        }

        /// <summary>
        /// llama al frame del inicio de sesion con facebook
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            frmFLog frm = new frmFLog();
            frm.ShowDialog();
            UsBol.user = frm.GetUsuario();
            pictureBox4.ImageLocation = UsBol.user.UrlFoto?.ToString();
        }

        /// <summary>
        /// valida si el usuario es correcto y abre el frame de las salas 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!UsBol.user.ID_Cuenta.Equals(null))
                {
                     UsBol.user = UsBol.Login(UsBol.user);
                    frmSaldoYSala frm = new frmSaldoYSala(UsBol);
                    Hide();
                    frm.ShowDialog();
                    Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("El Login no se ralizó correctamente, por favor intente de nuevo");
                UsBol.user = null;
                Console.WriteLine(ex);
            }
        }
    }
}
