﻿namespace BlckJackEsteban
{
    partial class frmFLog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.navWeb = new System.Windows.Forms.WebBrowser();
            this.SuspendLayout();
            // 
            // navWeb
            // 
            this.navWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.navWeb.Location = new System.Drawing.Point(0, 0);
            this.navWeb.MinimumSize = new System.Drawing.Size(20, 20);
            this.navWeb.Name = "navWeb";
            this.navWeb.Size = new System.Drawing.Size(800, 450);
            this.navWeb.TabIndex = 0;
            // 
            // frmFLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.navWeb);
            this.Name = "frmFLog";
            this.Text = "frmFLog";
            this.Load += new System.EventHandler(this.frmFLog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.WebBrowser navWeb;
    }
}