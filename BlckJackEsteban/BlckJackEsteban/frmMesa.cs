﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Net;
using System.Windows.Forms;
using BlackJackEsteban.BOL;
using BlackJackEsteban.ENTITIES;

namespace BlckJackEsteban
{
    public partial class frmMesa : Form
    {
        UsuarioBOL usBol;
        AppBOL Log;
        EUrls Urls;
        DeckOfCardsEntities.RootobjectB baraja;
        bool turno;
        List<int> ganado;
        List<int> jugada;
        bool Plantar;

        public frmMesa(UsuarioBOL usBol)
        {
            InitializeComponent();
            Log = new AppBOL();
            Urls = new EUrls();
            this.usBol = usBol;
            usBol.user.Cartas = new List<DeckOfCardsEntities.Card>();
            turno = false;
            ganado = new List<int>();
            jugada = new List<int>();
            Plantar = false;
        }

        private void frmMesa_Load(object sender, EventArgs e)
        {
            usBol.user.part_act = Log.ActualizarPartida(usBol.user.part_act);
            PedirBaraja();
            baraja = Log.PedirBaraja(usBol.user, baraja);
            ganado.Add(usBol.user.Saldo);
            jugada.Add(0);
            usBol.user.Cartas.Add(PedirCarta());
            usBol.user.Cartas.Add(PedirCarta());
            //if (pictureBox1.Image == null && usBol.user.Cartas.Count == 1)
            //{
            //    pictureBox1.ImageLocation = usBol.user.Cartas[0].image;
            //}
            //else if (pictureBox2.Image == null && usBol.user.Cartas.Count == 2)
            //{
            //    pictureBox2.ImageLocation = usBol.user.Cartas[1].image;
            //}
            //Barajar();
            Imagenes();
            //timer1.Start();
            NombresYApuestas();
        }

        /// <summary>
        /// carga los nombres y todos los datos necesarios para para la interfaz
        /// </summary>
        private void NombresYApuestas()
        {
            pictureBox1.ImageLocation = usBol.user.Cartas[0].image;
            pictureBox2.ImageLocation = usBol.user.Cartas[1].image;
            
            groupBox1.Text = usBol.user.Nombre;
            label7.Text = "Tu Apuesta: " + usBol.user.Apuesta;
            Turno();
            
            try
            {
                groupBox2.Text = usBol.user.part_act.Jugadores[0].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[1].Nombre : usBol.user.part_act.Jugadores[0].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                groupBox3.Text = usBol.user.part_act.Jugadores[1].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[2].Nombre : usBol.user.part_act.Jugadores[1].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                groupBox4.Text = usBol.user.part_act.Jugadores[2].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[3].Nombre : usBol.user.part_act.Jugadores[2].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                groupBox5.Text = usBol.user.part_act.Jugadores[3].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[4].Nombre : usBol.user.part_act.Jugadores[3].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                groupBox6.Text = usBol.user.part_act.Jugadores[4].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[5].Nombre : usBol.user.part_act.Jugadores[4].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            try
            {
                groupBox7.Text = usBol.user.part_act.Jugadores[5].ID_Cuenta == usBol.user.ID_Cuenta ? usBol.user.part_act.Jugadores[6].Nombre : usBol.user.part_act.Jugadores[5].Nombre;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// se encarga de acomodar la resolucion de la imagen de la interfaz
        /// </summary>
        private void Imagenes()
        {
            pbMesa.Image = Log.ResizeImage(Properties.Resources._02a5303d_d82d_4f97_8136_dff51fc68b13, new Size(pbMesa.Height, pbMesa.Width));
        }

        /// <summary>
        /// barajar la baraja
        /// </summary>
        private void Barajar()
        {
            string res = Log.GetJson(Urls.Genrico + baraja.deck_id + Urls.Barajar);
            baraja = res.ParseJSON<DeckOfCardsEntities.RootobjectB>();
        }

        /// <summary>
        /// pide las cartas 
        /// </summary>
        /// <returns></returns>
        private DeckOfCardsEntities.Card PedirCarta()
        {
            string res = Log.GetJson(Urls.Genrico + baraja.deck_id + Urls.PedirCarta);
            DeckOfCardsEntities.RootobjectC carta = res.ParseJSON<DeckOfCardsEntities.RootobjectC>();
            DeckOfCardsEntities.Card[] cartas = carta.cards;
            Console.WriteLine(cartas[cartas.Length-1].value);
            return cartas[0];
        }

        /// <summary>
        /// se encarga de pedir la baraja si es necesario
        /// </summary>
        private void PedirBaraja()
        {
            string res = Log.GetJson(Urls.BarajaNueva);
            baraja = res.ParseJSON<DeckOfCardsEntities.RootobjectB>();
        }

        /// <summary>
        /// al cerrar la ventana permite el ingreso de otro usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMesa_FormClosing(object sender, FormClosingEventArgs e)
        {
            usBol.user = new EUsuario();
        }

        /// <summary>
        /// cierra la ventana permite el ingreso de otro usuario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            usBol.user = new EUsuario();
            Close();
        }

        /// <summary>
        /// se encarga de pedir cartas del maso y las muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPedir_Click(object sender, EventArgs e)
        {
            string temp = label8.Text.Replace("Turno: ", "");
            temp = usBol.user.Nombre;
            if (temp.Equals(usBol.user.Nombre))
            {
                try
                {
                    usBol.user.Cartas.Add(PedirCarta());

                    if (pictureBox1.Image == null && usBol.user.Cartas.Count == 1)
                    {
                        pictureBox1.ImageLocation = usBol.user.Cartas[0].image;
                    }
                    else if (pictureBox2.Image == null && usBol.user.Cartas.Count == 2)
                    {
                        pictureBox2.ImageLocation = usBol.user.Cartas[1].image;
                    }
                    else if (pictureBox3.Image == null && usBol.user.Cartas.Count == 3)
                    {
                        pictureBox3.ImageLocation = usBol.user.Cartas[2].image;
                    }
                    else if (pictureBox4.Image == null && usBol.user.Cartas.Count == 4)
                    {
                        pictureBox4.ImageLocation = usBol.user.Cartas[3].image;
                    }
                    else if (pictureBox5.Image == null && usBol.user.Cartas.Count == 5)
                    {
                        pictureBox5.ImageLocation = usBol.user.Cartas[4].image;
                    }


                    else if (pictureBox36.Image == null && usBol.user.Cartas.Count == 10)
                    {
                        pictureBox36.ImageLocation = usBol.user.Cartas[9].image;
                    }
                    else if (pictureBox37.Image == null && usBol.user.Cartas.Count == 9)
                    {
                        pictureBox37.ImageLocation = usBol.user.Cartas[8].image;
                    }
                    else if (pictureBox38.Image == null && usBol.user.Cartas.Count == 8)
                    {
                        pictureBox38.ImageLocation = usBol.user.Cartas[7].image;
                    }
                    else if (pictureBox39.Image == null && usBol.user.Cartas.Count == 7)
                    {
                        pictureBox39.ImageLocation = usBol.user.Cartas[6].image;
                    }
                    else if (pictureBox40.Image == null && usBol.user.Cartas.Count == 6)
                    {
                        pictureBox40.ImageLocation = usBol.user.Cartas[5].image;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// inhabilita la accion de pedir o doblar si es el turno del usuario y si cuenta con 2 cartas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPlantarse_Click(object sender, EventArgs e)
        {
            string temp = label8.Text.Replace("Turno: ", "");
            if (temp.Equals(usBol.user.Nombre))
            {
                if (usBol.user.Cartas.Count>1)
                {
                    btnDoblar.Enabled = false;
                    btnPedir.Enabled = false;
                    Plantar = true;
                }
            }
        }

        /// <summary>
        /// se encarga de hacer el acto de doblar
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDoblar_Click(object sender, EventArgs e)
        {
            string temp = label8.Text.Replace("Turno: ", "");
            if (temp.Equals(usBol.user.Nombre) && usBol.user.Apuesta * 2 < usBol.user.Saldo)
            {
                usBol.user.Apuesta = usBol.user.Apuesta * 2;
                btnPedir_Click(null, null);
                btnPlantarse_Click(null, null);
            }
        }

        /// <summary>
        /// se encarga del juego en tiempo real
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer1_Tick(object sender, EventArgs e)
        {
            usBol.user.part_act = Log.ActualizarPartida(usBol.user.part_act);
            usBol.user.part_act.Cartas = Log.PedicCartasDealer(usBol.user.part_act);
            Turno();
            string temp = label8.Text.Replace("Turno: ", "");
            if (temp.Equals("Cupier"))
            {
                JuegaCupier();
                Validar();
            }
        }

        /// <summary>
        /// muestra y se encarga del turno
        /// </summary>
        private void Turno()
        {
            string name = String.Empty;
            switch (usBol.user.part_act.Turno)
            {
                case 1:
                    name = usBol.user.part_act.Jugadores[0].Nombre;
                    break;
                case 2:
                    name = usBol.user.part_act.Jugadores[1].Nombre;
                    break;
                case 3:
                    name = usBol.user.part_act.Jugadores[2].Nombre;
                    break;
                case 4:
                    name = usBol.user.part_act.Jugadores[3].Nombre;
                    break;
                case 5:
                    name = usBol.user.part_act.Jugadores[4].Nombre;
                    break;
                case 6:
                    name = usBol.user.part_act.Jugadores[5].Nombre;
                    break;
                case 7:
                    name = usBol.user.part_act.Jugadores[6].Nombre;
                    break;
                case 8:
                    name = "Cupier";
                    break;
                default:
                    break;
            }
            label8.Text = "Turno: " + name;
        }


        /// <summary>
        /// valida el gane
        /// </summary>
        private void Validar()
        {
            if (Plantar)
            {

            }
        }

        /// <summary>
        /// se encarga del juego del cupier
        /// </summary>
        private void JuegaCupier()
        {
            if (usBol.user.part_act.Cartas.Count>=0 || usBol.user.part_act.Cartas.Count<5)
            {
                int temp1 = cuenta1(usBol.user.part_act.Cartas);
                int temp2 = cuenta2(usBol.user.part_act.Cartas);
                if (temp1==temp2)
                {

                    if (temp1<=16)
                    {
                        DeckOfCardsEntities.Card temp = PedirCarta();
                        usBol.user.part_act.Cartas.Add(temp.value);
                    }
                    else if (temp1>16||temp1==17)
                    {

                    }

                }
                else if (temp2>temp1)
                {
                    if (temp2 <= 16)
                    {
                        DeckOfCardsEntities.Card temp = PedirCarta();
                        usBol.user.part_act.Cartas.Add(temp.value);
                    }
                    else if (temp2 > 16 || temp2 == 17)
                    {

                    }
                }
            }
        }

        private int cuenta2(List<string> cartas)
        {
            int res = 0;
            foreach (var i in cartas)
            {
                switch (i)
                {
                    case "ACE":
                        res += 11;
                        break;
                    case "2":
                        res += 2;
                        break;
                    case "3":
                        res += 3;
                        break;
                    case "4":
                        res += 4;
                        break;
                    case "5":
                        res += 5;
                        break;
                    case "6":
                        res += 6;
                        break;
                    case "7":
                        res += 7;
                        break;
                    case "8":
                        res += 8;
                        break;
                    case "9":
                        res += 9;
                        break;
                    case "10":
                        res += 10;
                        break;
                    case "JACK":
                        res += 10;
                        break;
                    case "QUEEN":
                        res += 10;
                        break;
                    case "KING":
                        res += 10;
                        break;
                    default:
                        break;
                }
            }
            return res;
        }

        private int cuenta1(List<string> cartas)
        {
            int res = 0;
            foreach (var i in cartas)
            {
                switch (i)
                {
                    case "ACE":
                        res += 1;
                        break;
                    case "2":
                        res += 2;
                        break;
                    case "3":
                        res += 3;
                        break;
                    case "4":
                        res += 4;
                        break;
                    case "5":
                        res += 5;
                        break;
                    case "6":
                        res += 6;
                        break;
                    case "7":
                        res += 7;
                        break;
                    case "8":
                        res += 8;
                        break;
                    case "9":
                        res += 9;
                        break;
                    case "10":
                        res += 10;
                        break;
                    case "JACK":
                        res += 10;
                        break;
                    case "QUEEN":
                        res += 10;
                        break;
                    case "KING":
                        res += 10;
                        break;
                    default:
                        break;
                }
            }
            return res;
        }

        /// <summary>
        /// muestra el frame de apuestas si esta al principio de la partida
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (usBol.user.Cartas.Count<3)
            {
                frmApuesta frm = new frmApuesta(usBol);
                frm.ShowDialog();
                label7.Text = "Tu Apuesta: " + usBol.user.Apuesta;
            }
            else
            {
                MessageBox.Show("Lo sentimos, en este momento no puede ingresar apuestas", "Mesa", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            }
        }

        /// <summary>
        /// cambia de turno
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            Log.TerminarTurno(usBol.user.part_act);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Log.DejarPartida(usBol.user);
            Close();
        }
    }
}
