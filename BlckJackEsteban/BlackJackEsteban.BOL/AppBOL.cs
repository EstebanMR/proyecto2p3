﻿using BlackJackEsteban.DAL;
using BlackJackEsteban.ENTITIES;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace BlackJackEsteban.BOL
{
    public class AppBOL
    {
        /// <summary>
        /// busca un puerto para iniciar sesion con google
        /// </summary>
        /// <returns>puerto</returns>
        public static int GetRandomUnusedPort()
        {
            var listener = new TcpListener(IPAddress.Loopback, 0);
            listener.Start();
            var port = ((IPEndPoint)listener.LocalEndpoint).Port;
            listener.Stop();
            return port;
        }

        /// <summary>
        /// se encarga de decodificar una respuesta de google
        /// </summary>
        /// <param name="inputStirng">respuesta de google</param>
        /// <returns>el resultado de la decodificacion</returns>
        public static byte[] sha256(string inputStirng)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(inputStirng);
            SHA256Managed sha256 = new SHA256Managed();
            return sha256.ComputeHash(bytes);
        }

        /// <summary>
        /// llama al dal para traer los datos de la partida en tienpo real
        /// </summary>
        /// <param name="part_act">datos de la partida a actualizar</param>
        /// <returns>partida</returns>
        public ESala ActualizarPartida(ESala part_act)
        {
            return new AppDAL().ActualizarPartida(part_act);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idp">identificador de la partida</param>
        /// <param name="idu">identificador del usuario</param>
        /// <param name="cupo">cupo en que se encuentra el usuario</param>
        public void InsertarEnPartida(int idp, int idu, int cupo)
        {
            new AppDAL().InsertarUsuario(idp, idu, cupo);
        }

        /// <summary>
        /// se encarga de la baraja en la partida
        /// </summary>
        /// <param name="us">usuario</param>
        /// <param name="deck">id del deck</param>
        /// <returns>una baraja</returns>
        public DeckOfCardsEntities.RootobjectB PedirBaraja(EUsuario us, DeckOfCardsEntities.RootobjectB deck)
        {
            EUrls Urls = new EUrls();
            string temp = new AppDAL().PedirBaraja(us, deck);
            DeckOfCardsEntities.RootobjectB baraja = new DeckOfCardsEntities.RootobjectB();
            if (!temp.Equals(String.Empty))
            {
                string res = GetJson(Urls.Genrico + temp + Urls.Barajar);
                baraja = res.ParseJSON<DeckOfCardsEntities.RootobjectB>();
            }
            return baraja;
        }

        /// <summary>
        /// busca partida
        /// </summary>
        /// <returns>una sala</returns>
        public ESala BuscarPartida()
        {
            return new AppDAL().PedirSalaUsuario();

        }

        /// <summary>
        /// convierte un array de byte a string
        /// </summary>
        /// <param name="buffer">array de bytes</param>
        /// <returns></returns>
        public string base64urlencodeNoPadding(byte[] buffer)
        {
            string base64 = Convert.ToBase64String(buffer);

            // Converts base64 to base64url.
            base64 = base64.Replace("+", "-");
            base64 = base64.Replace("/", "_");
            // Strips padding.
            base64 = base64.Replace("=", "");

            return base64;
        }

        /// <summary>
        /// descarga el jason con la informacion de la url
        /// </summary>
        /// <param name="Url">url a descargar el contenido</param>
        /// <returns>retorna un string con el json</returns>
        public string GetJson(string Url)
        {
            try
            {
                return new WebClient().DownloadString(Url);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// crea o busca la sala privada
        /// </summary>
        /// <param name="u">usuario con la partida</param>
        /// <returns>la sala privada</returns>
        public ESala CrearPrivada(EUsuario u)
        {
            ESala salap = new AppDAL().PedirSalaPrivada(u);
            //Console.WriteLine(salap.ID+"/"+ salap.Password);
            new AppDAL().EnviarCorreo(salap, u);
            return salap;
        }

        /// <summary>
        /// crea un array de bytes
        /// </summary>
        /// <param name="length"> tamaño del array</param>
        /// <returns>el array</returns>
        public string randomDataBase64url(int length)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] bytes = new byte[length];
            rng.GetBytes(bytes);
            return base64urlencodeNoPadding(bytes);
        }

        /// <summary>
        /// reescala imagen
        /// </summary>
        /// <param name="imgToResize">imagen a reescalar</param>
        /// <param name="size">tamaño a reescalar</param>
        /// <returns>imagen reescalada</returns>
        public Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        /// <summary>
        /// se encarga de buscar una de las salas privadas con datos de la sala
        /// </summary>
        /// <param name="id">id de la sala</param>
        /// <param name="con">contraseña de la sala</param>
        /// <returns>sala</returns>
        public ESala BuscarPrivada(int id, string con)
        {
            return new AppDAL().BuscarSala(id, con);
        }

        /// <summary>
        /// cambio de turno
        /// </summary>
        /// <param name="sal">sala</param>
        public void TerminarTurno(ESala sal)
        {
            new AppDAL().TerminarTurno(sal);
        }

        /// <summary>
        /// pide las cartas del dealer
        /// </summary>
        /// <param name="part_act">la partida </param>
        /// <returns>las cartas</returns>
        public List<string> PedicCartasDealer(ESala part_act)
        {
            return new AppDAL().PedirCartasDealer(part_act);
        }

        public void DejarPartida(EUsuario user)
        {
            int cont = 0;
            foreach (var i in user.part_act.Jugadores)
            {
                cont++;
                if (i.ID==user.ID)
                {
                    break;
                }
            }
            string res = "j" + cont;
            new AppDAL().DejarPartida(user, res);
        }
    }
}
