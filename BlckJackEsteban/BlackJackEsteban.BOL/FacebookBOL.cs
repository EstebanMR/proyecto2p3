﻿using Facebook;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.BOL
{
    public class FacebookBOL
    {
        private string applicationId = "455956378383081";
        private string extendedPermissionsNeeded = "public_profile,email";

        /// <summary>
        /// genera la url para iniciar sesion con facebook
        /// </summary>
        /// <returns>la url</returns>
        public string GenerateLoginUrl()
        {
            dynamic parameters = new ExpandoObject();
            parameters.client_id = applicationId;
            parameters.redirect_uri = "https://www.facebook.com/connect/login_success.html";
            parameters.response_type = "token";
            parameters.display = "popup";
            parameters.scope = extendedPermissionsNeeded;
            var fb = new FacebookClient();
            Uri loginUri = fb.GetLoginUrl(parameters);
            return loginUri.AbsoluteUri;
        }
    }
}
