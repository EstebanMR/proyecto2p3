﻿using BlackJackEsteban.ENTITIES;
using BlackJackEsteban.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.BOL
{
    public class UsuarioBOL
    {
        public EUsuario user = new EUsuario();

        /// <summary>
        /// se encarga de buscar o ingresar el usuario
        /// </summary>
        /// <param name="us">usuario</param>
        /// <returns>el usuario completo</returns>
        public EUsuario Login(EUsuario us)
        {
            return new UsuarioDAL().Login(us);
        }

        /// <summary>
        /// sube las apuestas a la base
        /// </summary>
        /// <param name="value">valor de la apuesta</param>
        /// <param name="us">usuario</param>
        public void IngreApuesta(decimal value, EUsuario us)
        {
            new UsuarioDAL().IngreApuesta(value, us);
        }

        /// <summary>
        /// aumenta el saldo del usuario
        /// </summary>
        public void AumentarSaldo()
        {
            new UsuarioDAL().AumentarSaldo(user);
        }

        /// <summary>
        /// disminuye el saldo por las apuestas
        /// </summary>
        /// <param name="restante">valor para restar al saldo</param>
        public void QuitarSaldo(int res)
        {
            new UsuarioDAL().QuitarSaldo(res, user);

        }
    }
}

