﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.ENTITIES
{
    public class EUsuario
    {
        public int ID { get; set; }
        public string ID_Cuenta { get; set; }
        public string Nombre { get; set; }
        public string UrlFoto { get; set; }
        public string Email { get; set; }
        public string Plataforma { get; set; }
        public int Saldo { get; set; }
        public int Partidas { get; set; }
        public int Apuesta { get; set; }
        public List<DeckOfCardsEntities.Card> Cartas { get; set; }
        public bool Gana { get; set; }
        public ESala part_act { get; set; }
        public int Posicion { get; set; }

        //public DeckOfCardsEntities. MyProperty { get; set; }
    }
}
