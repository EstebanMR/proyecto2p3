﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.ENTITIES
{
    public class ESala
    {

        public int ID { get; set; }
        public string Password { get; set; }
        public List<EUsuario> Jugadores { get; set; }
        public int Turno { get; set; }
        public string Baraja { get; set; }
        public int cupos { get; set; }
        public List<String> Cartas { get; set; }
        public int Valor { get; set; }
    }
}
