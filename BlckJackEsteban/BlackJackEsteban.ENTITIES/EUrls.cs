﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.ENTITIES
{
    public class EUrls
    {
        public string BarajaNueva = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1";
        public string Genrico = "https://deckofcardsapi.com/api/deck/";
        public string PedirCarta = "/draw/?count=1";
        public string Barajar = "/shuffle/";
    }
}
