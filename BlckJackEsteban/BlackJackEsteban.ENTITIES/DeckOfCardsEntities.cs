﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJackEsteban.ENTITIES
{
    public class DeckOfCardsEntities
    {

        public class RootobjectB
        {
            public bool shuffled { get; set; }
            public string deck_id { get; set; }
            public int remaining { get; set; }
            public bool success { get; set; }
        }

        public class RootobjectC
        {
            public string deck_id { get; set; }
            public int remaining { get; set; }
            public Card[] cards { get; set; }
            public bool success { get; set; }
        }

        public class Card
        {
            public ImagesC images { get; set; }
            public string suit { get; set; }
            public string value { get; set; }
            public string code { get; set; }
            public string image { get; set; }
        }

        public class ImagesC
        {
            public string png { get; set; }
            public string svg { get; set; }
        }

    }
}
